package ru.malakhov.tm.exception.system;

import ru.malakhov.tm.exception.AbstractException;

public final class UnknownCommandException extends AbstractException {

    public UnknownCommandException(final String command) {
        super("Error! Unknown ``" + command + "`` command...");
    }

}