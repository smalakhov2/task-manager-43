package ru.malakhov.tm.endpoint.soap;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.4.2
 * 2021-03-14T01:00:55.410+03:00
 * Generated source version: 3.4.2
 *
 */
@WebService(targetNamespace = "http://soap.endpoint.tm.malakhov.ru/", name = "AuthSoapEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface AuthSoapEndpoint {

    @WebMethod
    @RequestWrapper(localName = "login", targetNamespace = "http://soap.endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.soap.Login")
    @ResponseWrapper(localName = "loginResponse", targetNamespace = "http://soap.endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.soap.LoginResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.malakhov.tm.endpoint.soap.Result login(

        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login,
        @WebParam(name = "password", targetNamespace = "")
        java.lang.String password
    );

    @WebMethod
    @RequestWrapper(localName = "logout", targetNamespace = "http://soap.endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.soap.Logout")
    @ResponseWrapper(localName = "logoutResponse", targetNamespace = "http://soap.endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.soap.LogoutResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.malakhov.tm.endpoint.soap.Result logout()
;

    @WebMethod
    @RequestWrapper(localName = "registration", targetNamespace = "http://soap.endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.soap.Registration")
    @ResponseWrapper(localName = "registrationResponse", targetNamespace = "http://soap.endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.soap.RegistrationResponse")
    public void registration(

        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login,
        @WebParam(name = "password", targetNamespace = "")
        java.lang.String password
    );
}
