package ru.malakhov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.event.ConsoleEvent;

@Component
public class ProjectClearListener extends AbstractProjectListener {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String command() {
        return "project-clear";
    }

    @Override
    public @NotNull String description() {
        return "Remove all Projects.";
    }

    @Override
    @EventListener(condition = "@projectClearListener.command() == #event.command")
    public void handle(final ConsoleEvent event) {
        sessionService.setListCookieRowRequest(projectEndpoint);
        System.out.println("[CLEAR PROJECTS]");
        projectEndpoint.deleteAll();
        System.out.println("[OK]");
    }

}