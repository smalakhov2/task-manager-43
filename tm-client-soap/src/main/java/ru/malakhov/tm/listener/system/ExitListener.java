package ru.malakhov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.listener.AbstractListener;

@Component
public class ExitListener extends AbstractListener {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String command() {
        return "exit";
    }

    @Override
    public @NotNull String description() {
        return "Close application.";
    }

    @Override
    @EventListener(condition = "@exitListener.command() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.exit(0);
    }

}
