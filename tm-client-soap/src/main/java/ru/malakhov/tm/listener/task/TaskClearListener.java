package ru.malakhov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.event.ConsoleEvent;

@Component
public class TaskClearListener extends AbstractTaskListener {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String command() {
        return "task-clear";
    }

    @Override
    public @NotNull String description() {
        return "Remove all tasks.";
    }

    @Override
    @EventListener(condition = "@taskClearListener.command() == #event.command")
    public void handle(final ConsoleEvent event) {
        sessionService.setListCookieRowRequest(taskEndpoint);
        System.out.println("[CLEAR TASKS]");
        taskEndpoint.deleteAllTasks();
        System.out.println("[OK]");
    }

}