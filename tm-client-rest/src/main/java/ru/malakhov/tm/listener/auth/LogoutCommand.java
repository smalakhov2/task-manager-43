package ru.malakhov.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.malakhov.tm.api.endpoint.IAuthRestEndpoint;
import ru.malakhov.tm.event.ConsoleEvent;
import ru.malakhov.tm.listener.AbstractListener;

@Component
public class LogoutCommand extends AbstractListener {

    @Autowired
    private IAuthRestEndpoint authRestEndpoint;

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String command() {
        return "logout";
    }

    @Override
    public @NotNull String description() {
        return "Logout.";
    }

    @Async
    @Override
    @EventListener(condition = "@logoutListener.command() == #event.command")
    public void handle(final ConsoleEvent event) {
        System.out.println("[LOGout]");
        authRestEndpoint.logout();
        System.out.println("[OK]");
    }

}