package ru.malakhov.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.malakhov.tm.bootstrap.Bootstrap;
import ru.malakhov.tm.config.ClientConfiguration;

public class App {

    public static void main( String[] args ) {
        final @NotNull AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ClientConfiguration.class);
        final @NotNull Bootstrap bootstrap = context.getBean(Bootstrap.class);
        context.registerShutdownHook();
        bootstrap.run(args);
    }

}