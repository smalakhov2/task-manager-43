package ru.malakhov.tm.api.endpoint;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.malakhov.tm.dto.ProjectDTO;

@RequestMapping("/api/project")
public interface IProjectRestEndpoint {

    static IProjectRestEndpoint projectClient(final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(IProjectRestEndpoint.class, baseUrl);
    }

    @PostMapping
    void create(@RequestBody String name);

    @PutMapping
    void update(@RequestBody ProjectDTO projectDTO);

    @GetMapping("/${id}")
    ProjectDTO findOneByIdDTO(@PathVariable("id") String id);

    @GetMapping("/exist/${id}")
    boolean existsById(@PathVariable("id") String id);

    @DeleteMapping("/${id}")
    void deleteOneById(@PathVariable("id") String id);

}