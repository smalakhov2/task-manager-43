package ru.malakhov.tm.api.endpoint;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/api/auth")
public interface IAuthRestEndpoint {

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    boolean login(
            @RequestParam("login") final String login,
            @RequestParam("password") final String password
    );

    @GetMapping(value = "/logout")
    void logout();
}