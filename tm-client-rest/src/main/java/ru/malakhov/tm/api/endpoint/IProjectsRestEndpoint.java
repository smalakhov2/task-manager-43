package ru.malakhov.tm.api.endpoint;

import feign.Feign;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.malakhov.tm.dto.ProjectDTO;
import ru.malakhov.tm.entity.Project;

import java.util.List;

@RequestMapping("/api/projects")
public interface IProjectsRestEndpoint {

    static IProjectsRestEndpoint projectsClient(final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(IProjectsRestEndpoint.class, baseUrl);
    }

    @GetMapping
    List<ProjectDTO> getListDTO();

    @PostMapping
    List<Project> saveAll(@RequestBody List<Project> list);

    @DeleteMapping("/")
    void deleteAll(@RequestBody List<Project> list);

    @DeleteMapping("/all")
    void deleteAll();

}