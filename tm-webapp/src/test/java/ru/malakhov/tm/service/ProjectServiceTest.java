package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.config.WebApplicationConfiguration;
import ru.malakhov.tm.dto.ProjectDTO;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.repository.entity.IUserRepository;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebApplicationConfiguration.class)
@Transactional
public class ProjectServiceTest {

    @Autowired
    private IProjectService projectService;

    @Autowired
    private IUserRepository userRepository;

    final @NotNull User user1 = new User();
    final @NotNull User user2 = new User();

    @Before
    public void initData() {
        userRepository.save(user1);
        userRepository.save(user2);
    }

    @After
    public void clearData() {
        projectService.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    public void createTest() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        projectService.create(user1.getId(), "Test");
        Assert.assertEquals("Test", projectService.findAll().get(0).getName());
    }

    @Test
    public void updateDTOTest() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        projectService.create(user1.getId(), "Test");
        Assert.assertEquals("Test", projectService.findAll().get(0).getName());
        final @NotNull ProjectDTO projectDTO = projectService.findAllDTOByUserId(user1.getId()).get(0);
        projectDTO.setName("projectDTO");
        projectService.updateDTO(user1.getId(), projectDTO);
        Assert.assertEquals("projectDTO", projectService.findAllDTOByUserId(user1.getId()).get(0).getName());
    }

    @Test
    public void findAllTest() {
        projectService.create(user1.getId(), "Test");
        projectService.create(user1.getId(), "Test2");
        Assert.assertEquals(2, projectService.findAll().size());
    }

    @Test
    public void findAllDTOByUserIdTest() {
        projectService.create(user1.getId(), "Test");
        projectService.create(user1.getId(), "Test2");
        projectService.create(user2.getId(), "Test3");
        Assert.assertEquals(2, projectService.findAllDTOByUserId(user1.getId()).size());
        Assert.assertEquals(1, projectService.findAllDTOByUserId(user2.getId()).size());
    }

    @Test
    public void deleteAllTest() {
        projectService.create(user1.getId(), "Test");
        projectService.create(user1.getId(), "Test2");
        Assert.assertEquals(2, projectService.findAllDTOByUserId(user1.getId()).size());
        projectService.deleteAll();
        Assert.assertEquals(0, projectService.findAllDTOByUserId(user1.getId()).size());
    }

    @Test
    public void deleteAllByUserIdTest() {
        projectService.create(user1.getId(), "Test");
        projectService.create(user1.getId(), "Test2");
        projectService.create(user2.getId(), "Test3");
        Assert.assertEquals(2, projectService.findAllDTOByUserId(user1.getId()).size());
        Assert.assertEquals(1, projectService.findAllDTOByUserId(user2.getId()).size());
        projectService.deleteAllByUserId(user1.getId());
        Assert.assertEquals(0, projectService.findAllDTOByUserId(user1.getId()).size());
        projectService.deleteAllByUserId(user2.getId());
        Assert.assertEquals(0, projectService.findAllDTOByUserId(user2.getId()).size());

    }

    @Test
    public void findOneByIdDTOTest() {
        projectService.create(user1.getId(), "Test");
        final @NotNull ProjectDTO projectDTO = projectService.findAllDTOByUserId(user1.getId()).get(0);
        final @NotNull String id = projectDTO.getId();
        projectDTO.setName("projectDTO");
        projectService.updateDTO(user1.getId(), projectDTO);
        Assert.assertEquals(projectService.findOneByIdDTO(user1.getId(),id).getName(), projectDTO.getName());
    }

    @Test
    public void removeOneByIdTest() {
        projectService.create(user1.getId(), "Test");
        final @NotNull ProjectDTO projectDTO = projectService.findAllDTOByUserId(user1.getId()).get(0);
        final @NotNull String id = projectDTO.getId();
        Assert.assertFalse(projectService.findAll().isEmpty());
        projectService.removeOneById(user1.getId(), id);
        Assert.assertTrue(projectService.findAll().isEmpty());
    }

    @Test
    public void existsByUserIdAndIdTest() {
        projectService.create(user1.getId(), "Test");
        final @NotNull ProjectDTO projectDTO = projectService.findAllDTOByUserId(user1.getId()).get(0);
        final @NotNull String id = projectDTO.getId();
        Assert.assertTrue(projectService.existsByUserIdAndId(user1.getId(), id));
        projectService.removeOneById(user1.getId(), id);
        Assert.assertFalse(projectService.existsByUserIdAndId(user1.getId(), id));
    }

}
