package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.config.WebApplicationConfiguration;
import ru.malakhov.tm.dto.UserDTO;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebApplicationConfiguration.class)
@Transactional
public class UserServiceTest {

    @Autowired
    private IUserService userService;

    @After
    public void clearData() {
        userService.deleteAll();
    }

    @Test
    public void createTest() {
        userService.create("test", "password");
        Assert.assertEquals("test", userService.findByLoginDTO("test").getLogin());
    }

    @Test
    public void updateDTOTest() {
        userService.create("test", "password");
        final @NotNull UserDTO userDTO = userService.findByLoginDTO("test");
        userDTO.setFirstName("test_name");
        userService.updateDTO(userDTO);
        Assert.assertEquals("test_name", userService.findByLoginDTO("test").getFirstName());
    }

    @Test
    public void deleteAll() {
        userService.create("test", "password1");
        userService.create("test2", "password2");
        userService.create("test3", "password3");
        Assert.assertEquals(3, userService.findAll().size());
        userService.deleteAll();
        Assert.assertEquals(0, userService.findAll().size());

    }

    @Test
    public void findAllTest() {
        Assert.assertTrue(userService.findAll().isEmpty());
        userService.create("test", "password1");
        userService.create("test2", "password2");
        userService.create("test3", "password3");
        Assert.assertEquals(3, userService.findAll().size());
    }

    @Test
    public void findByIdTest() {
        userService.create("test", "password");
        final @NotNull UserDTO userDTO = userService.findByLoginDTO("test");
        final @NotNull String id = userDTO.getId();
        Assert.assertEquals("test", userService.findById(id).getLogin());
    }

    @Test
    public void findByLoginDTOTest() {
        userService.create("test", "password");
        final @NotNull UserDTO userDTO = userService.findByLoginDTO("test");
        userDTO.setFirstName("test_name");
        userService.updateDTO(userDTO);
        Assert.assertEquals("test_name", userService.findByLoginDTO("test").getFirstName());
    }

    @Test
    public void profileTest() {
        userService.create("test", "password");
        final @NotNull UserDTO userDTO = userService.findByLoginDTO("test");
        final @NotNull String id = userDTO.getId();
        Assert.assertEquals("test", userService.profile(id).getLogin());
    }

}