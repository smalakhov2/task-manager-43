package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.api.service.IAuthService;
import ru.malakhov.tm.config.WebApplicationConfiguration;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.enumeration.Role;
import ru.malakhov.tm.repository.entity.IUserRepository;


@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebApplicationConfiguration.class)
@Transactional
public class AuthServiceTest {

    @Autowired
    private IAuthService authService;

    @Autowired
    private IUserRepository userRepository;

    @After
    @Transactional
    public void clearData() {
        userRepository.deleteAll();
    }

    @Test
    public void checkRolesTest() {
        final @NotNull User user1 = new User();
        final @NotNull User user2 = new User();
        user2.setRole(Role.ADMIN);

        userRepository.save(user1);
        userRepository.save(user2);

        Role[] rolesUser = new Role[1];
        rolesUser[0] = Role.USER;

        Role[] rolesAdmin = new Role[1];
        rolesAdmin[0] = Role.ADMIN;

        Assert.assertTrue(authService.checkRoles(user1.getId(), rolesUser));
        Assert.assertTrue(authService.checkRoles(user2.getId(), rolesAdmin));
    }

    @Test
    public void registrationTest() {
        Assert.assertNull(userRepository.findOneByLogin("test"));
        authService.registration("test", "test");
        Assert.assertNotNull(userRepository.findOneByLogin("test"));

    }

}