package ru.malakhov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.config.WebApplicationConfiguration;
import ru.malakhov.tm.dto.TaskDTO;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.repository.entity.IUserRepository;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebApplicationConfiguration.class)
public class TaskRepositoryDTOTest {

    @Autowired
    private ITaskRepositoryDTO taskRepositoryDTO;

    @Autowired
    private IUserRepository userRepository;

    final @NotNull User user1 = new User();
    final @NotNull User user2 = new User();

    @Before
    @Transactional
    public void initData() {
        userRepository.save(user1);
        userRepository.save(user2);
    }

    @After
    @Transactional
    public void clearData() {
        userRepository.deleteAll();
    }

    @Test
    @Transactional
    public void findAllByUserIdTest() {
        final @NotNull TaskDTO taskDTO1 = new TaskDTO();
        final @NotNull TaskDTO taskDTO2 = new TaskDTO();
        final @NotNull TaskDTO taskDTO3 = new TaskDTO();

        taskDTO1.setUserId(user1.getId());
        taskDTO2.setUserId(user1.getId());
        taskDTO3.setUserId(user2.getId());

        taskRepositoryDTO.save(taskDTO1);
        taskRepositoryDTO.save(taskDTO2);
        taskRepositoryDTO.save(taskDTO3);

        Assert.assertEquals(2, taskRepositoryDTO.findAllByUserId(user1.getId()).size());
        Assert.assertEquals(1, taskRepositoryDTO.findAllByUserId(user2.getId()).size());

        taskRepositoryDTO.delete(taskDTO1);
        taskRepositoryDTO.delete(taskDTO2);
        taskRepositoryDTO.delete(taskDTO3);
    }

    @Test
    public void findOneByUserIdAndIdTest() {
        final @NotNull TaskDTO taskDTO1 = new TaskDTO();
        final @NotNull TaskDTO taskDTO2 = new TaskDTO();

        taskDTO1.setUserId(user1.getId());
        taskDTO2.setUserId(user2.getId());

        taskRepositoryDTO.save(taskDTO1);
        taskRepositoryDTO.save(taskDTO2);

        Assert.assertEquals(taskDTO1.getId(), taskRepositoryDTO.findOneByUserIdAndId(user1.getId(), taskDTO1.getId()).getId());
        Assert.assertEquals(taskDTO2.getId(), taskRepositoryDTO.findOneByUserIdAndId(user2.getId(), taskDTO2.getId()).getId());

        taskRepositoryDTO.delete(taskDTO1);
        taskRepositoryDTO.delete(taskDTO2);
    }

    @Test
    public void existsByIdAndUserIdTest() {
        final @NotNull TaskDTO taskDTO1 = new TaskDTO();
        final @NotNull TaskDTO taskDTO2 = new TaskDTO();

        taskDTO1.setUserId(user1.getId());
        taskDTO2.setUserId(user2.getId());

        taskRepositoryDTO.save(taskDTO1);
        taskRepositoryDTO.save(taskDTO2);

        Assert.assertTrue(taskRepositoryDTO.existsByUserIdAndId(user1.getId(), taskDTO1.getId()));
        Assert.assertTrue(taskRepositoryDTO.existsByUserIdAndId(user2.getId(), taskDTO2.getId()));

        taskRepositoryDTO.delete(taskDTO1);
        taskRepositoryDTO.delete(taskDTO2);
    }

}