package ru.malakhov.tm.repository.entity;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.config.WebApplicationConfiguration;
import ru.malakhov.tm.entity.Project;
import ru.malakhov.tm.entity.User;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebApplicationConfiguration.class)
public class ProjectRepositoryTest {

    @Autowired
    private IProjectRepository projectRepository;

    @Autowired
    private IUserRepository userRepository;

    final @NotNull User user1 = new User();

    @Before
    @Transactional
    public void initData() {
        userRepository.save(user1);
    }

    @After
    @Transactional
    public void clearData() {
        userRepository.deleteAll();
        projectRepository.deleteAll();
    }


    @Test
    @Transactional
    public void deleteAllByUserIdTest() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());

        final @NotNull Project project1 = new Project();
        project1.setUser(user1);
        projectRepository.save(project1);

        final @NotNull Project project2 = new Project();
        project2.setUser(user1);
        projectRepository.save(project2);

        Assert.assertFalse(projectRepository.findAll().isEmpty());

        projectRepository.deleteAllByUserId(user1.getId());

        Assert.assertTrue(projectRepository.findAll().isEmpty());
    }

    @Test
    @Transactional
    public void deleteOneByUserIdAndIdTest() {
        Assert.assertTrue(projectRepository.findAll().isEmpty());

        final @NotNull Project project1 = new Project();
        project1.setUser(user1);
        projectRepository.save(project1);

        Assert.assertTrue(projectRepository.existsById(project1.getId()));

        projectRepository.deleteOneByUserIdAndId(user1.getId(), project1.getId());

        Assert.assertFalse(projectRepository.existsById(project1.getId()));
    }

}