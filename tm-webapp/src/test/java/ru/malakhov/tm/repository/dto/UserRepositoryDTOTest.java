package ru.malakhov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.config.WebApplicationConfiguration;
import ru.malakhov.tm.dto.UserDTO;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebApplicationConfiguration.class)
public class UserRepositoryDTOTest {

    @Autowired
    private IUserRepositoryDTO userRepositoryDTO;

    @Test
    @Transactional
    public void findOneByIdTest() {
        final @NotNull UserDTO userDTO1 = new UserDTO();
        final @NotNull UserDTO userDTO2 = new UserDTO();

        userRepositoryDTO.save(userDTO1);
        userRepositoryDTO.save(userDTO2);

        Assert.assertEquals(userDTO1.getId(), userRepositoryDTO.findOneById(userDTO1.getId()).getId());
        Assert.assertEquals(userDTO2.getId(), userRepositoryDTO.findOneById(userDTO2.getId()).getId());

        userRepositoryDTO.delete(userDTO1);
        userRepositoryDTO.delete(userDTO2);
    }

    @Test
    @Transactional
    public void findOneByLoginTest() {
        final @NotNull UserDTO userDTO1 = new UserDTO();
        userDTO1.setLogin("test1");
        final @NotNull UserDTO userDTO2 = new UserDTO();
        userDTO2.setLogin("test2");

        userRepositoryDTO.save(userDTO1);
        userRepositoryDTO.save(userDTO2);

        Assert.assertEquals(userDTO1.getId(), userRepositoryDTO.findOneByLogin("test1").getId());
        Assert.assertEquals(userDTO2.getId(), userRepositoryDTO.findOneByLogin("test2").getId());

        userRepositoryDTO.delete(userDTO1);
        userRepositoryDTO.delete(userDTO2);
    }

}