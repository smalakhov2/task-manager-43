package ru.malakhov.tm.repository.entity;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.config.WebApplicationConfiguration;
import ru.malakhov.tm.entity.User;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebApplicationConfiguration.class)
public class UserRepositoryTest {

    @Autowired
    private IUserRepository userRepository;

    final @NotNull User user = new User();


    @Before
    @Transactional
    public void initData() {
        user.setLogin("test");
        userRepository.save(user);
    }

    @After
    @Transactional
    public void clearData() {
        userRepository.deleteAll();
    }

    @Test
    public void findOneByIdTest() {
        Assert.assertEquals(user.getId(), userRepository.findOneById(user.getId()).getId());
    }

    @Test
    public void findOneByLoginTest() {
        Assert.assertEquals(user.getId(), userRepository.findOneByLogin("test").getId());
    }

    @Test
    @Transactional
    public void deleteByIdTest() {
        Assert.assertFalse(userRepository.findAll().isEmpty());
        userRepository.deleteById(user.getId());
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

}