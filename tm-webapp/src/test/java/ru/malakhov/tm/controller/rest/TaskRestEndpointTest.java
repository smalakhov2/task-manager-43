package ru.malakhov.tm.controller.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.Transactional;
import ru.malakhov.tm.controller.AbstractControllerTest;
import ru.malakhov.tm.api.service.ITaskService;
import ru.malakhov.tm.dto.TaskDTO;
import ru.malakhov.tm.repository.entity.IProjectRepository;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class TaskRestEndpointTest extends AbstractControllerTest {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectRepository projectRepository;

    @After
    public void clearTasks() {
        taskService.deleteAll();
    }

    @Test
    public void createTest() throws Exception {
        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        final ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString("task");

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders
                .post("/api/task/create")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(requestJson));

        result.andExpect(MockMvcResultMatchers.status().isOk());
        final TaskDTO taskFromBase = taskService.findAllDTOByUserId(user.getId()).get(0);
        Assert.assertEquals("\"task\"", taskFromBase.getName());
    }

    @Test
    @Transactional
    public void updateTest() throws Exception {
        final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setName("task");
        taskDTO.setDescription("task");
        taskDTO.setUserId(user.getId());
        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        final ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(taskDTO);

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders
                .put("/api/task")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(requestJson));

        result.andExpect(MockMvcResultMatchers.status().isOk());
        final TaskDTO taskFromBase = taskService.findOneByIdDTO(user.getId(), taskDTO.getId());
        Assert.assertEquals(taskDTO.getName(), taskFromBase.getName());
    }

    @Test
    public void findOneByIdDTOTest() throws Exception {
        final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setName("taskName");
        taskDTO.setDescription("taskDescription");
        taskDTO.setUserId(user.getId());
        taskService.updateDTO(user.getId(), taskDTO);

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/task/${id}", taskDTO.getId())
                .accept(MediaType.APPLICATION_JSON));

        result.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.name").value(taskDTO.getName()))
                .andExpect(jsonPath("$.description").value(taskDTO.getDescription()));

    }

    @Test
    public void existByIdTest() throws Exception {
        final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setName("taskName");
        taskDTO.setDescription("taskDescription");
        taskDTO.setUserId(user.getId());
        taskService.updateDTO(user.getId(), taskDTO);

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/task/exist/${id}", taskDTO.getId())
                .accept(MediaType.APPLICATION_JSON));

        result.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("true"));

        result = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/task/exist/${id}", "dfsdfsd")
                .accept(MediaType.APPLICATION_JSON));

        result.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("false"));
    }

    @Test
    @Transactional
    public void deleteOneByIdTest() throws Exception {
        final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setName("taskName");
        taskDTO.setDescription("taskDescription");
        taskDTO.setUserId(user.getId());
        taskService.updateDTO(user.getId(), taskDTO);
        Assert.assertNotNull(taskService.findOneByIdDTO(user.getId(), taskDTO.getId()));

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders
                .delete("/api/task/${id}", taskDTO.getId())
                .accept(MediaType.APPLICATION_JSON));

        result.andExpect(MockMvcResultMatchers.status().isOk());
        Assert.assertTrue(taskService.findAll().isEmpty());

    }

    @Test
    public void getListDTOTest() throws Exception {
        taskService.create(user.getId(), "task1");
        taskService.create(user.getId(), "task2");
        ResultActions result = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/tasks")
                .accept(MediaType.APPLICATION_JSON));

        result.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$..name", hasItem("task1")))
                .andExpect(jsonPath("$..name", hasItem("task2")));
    }

    @Test
    @Transactional
    public void deleteAllTest() throws Exception {
        taskService.create(user.getId(), "task1");
        taskService.create(user.getId(), "task2");
        taskService.create(user.getId(), "task3");
        ResultActions result = mockMvc.perform(MockMvcRequestBuilders
                .delete("/api/tasks/all")
                .accept(MediaType.APPLICATION_JSON));

        result.andExpect(MockMvcResultMatchers.status().isOk());
        Assert.assertEquals(0, taskService.findAll().size());
    }

}