package ru.malakhov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.malakhov.tm.dto.CustomUser;
import ru.malakhov.tm.dto.ProjectDTO;
import ru.malakhov.tm.api.service.IProjectService;

import java.util.List;

@Controller
public class ProjectsController {

    @Autowired
    private IProjectService projectService;

    @GetMapping("/projects")
    public ModelAndView index(@AuthenticationPrincipal CustomUser user) {
        final List<ProjectDTO> projects = projectService.findAllDTOByUserId(user.getUserId());
        return new ModelAndView("project/project-list", "projects", projects);
    }

}