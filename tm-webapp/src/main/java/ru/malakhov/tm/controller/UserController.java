package ru.malakhov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.malakhov.tm.dto.UserDTO;
import ru.malakhov.tm.api.service.IAuthService;

@Controller
public class UserController {

    @Autowired
    private IAuthService authService;


    @GetMapping("/registration")
    public ModelAndView registration(
    ) {
        final UserDTO userDTO = new UserDTO();
        return new ModelAndView("user/user-registration", "newUser", userDTO);
    }

    @PostMapping("/registration")
    public String registration(
            @ModelAttribute("newUser") UserDTO userDTO,
            BindingResult result
    ) {
        authService.registration(userDTO.getLogin(), userDTO.getPasswordHash());
        return "redirect:/login";
    }

}