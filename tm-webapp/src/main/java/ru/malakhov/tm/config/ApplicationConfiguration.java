package ru.malakhov.tm.config;

import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.WebApplicationInitializer;
import ru.malakhov.tm.endpoint.soap.AuthSoapEndpoint;
import ru.malakhov.tm.endpoint.soap.ProjectSoapEndpoint;
import ru.malakhov.tm.endpoint.soap.TaskSoapEndpoint;
import ru.malakhov.tm.endpoint.soap.UserSoapEndpoint;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.xml.ws.Endpoint;

@Configuration
@ComponentScan("ru.malakhov.tm")
public class ApplicationConfiguration implements WebApplicationInitializer {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public SpringBus cxf() {
        return new SpringBus();
    }
/*
    @Bean
    public Endpoint projectEndpointRegistry(final @NotNull ProjectSoapEndpoint projectSoapEndpoint, final @NotNull Bus bus) {
        final EndpointImpl endpoint = new EndpointImpl(bus, projectSoapEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint taskEndpointRegistry(final @NotNull TaskSoapEndpoint taskSoapEndpoint, final @NotNull Bus bus) {
        final EndpointImpl endpoint = new EndpointImpl(bus, taskSoapEndpoint);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint authEndpointRegistry(final @NotNull AuthSoapEndpoint authSoapEndpoint, final @NotNull Bus bus) {
        final EndpointImpl endpoint = new EndpointImpl(bus, authSoapEndpoint);
        endpoint.publish("/AuthEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint userEndpointRegistry(final @NotNull UserSoapEndpoint userSoapEndpoint, final @NotNull Bus bus) {
        final EndpointImpl endpoint = new EndpointImpl(bus, userSoapEndpoint);
        endpoint.publish("/UserEndpoint");
        return endpoint;
    }*/

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        final CXFServlet cxfServlet = new CXFServlet();
        final ServletRegistration.Dynamic dynamicCXF =
                servletContext.addServlet("cxfServlet", cxfServlet);
        dynamicCXF.addMapping("/ws/*");
        dynamicCXF.setLoadOnStartup(1);
    }

}