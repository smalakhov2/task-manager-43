package ru.malakhov.tm.repository.entity;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.malakhov.tm.entity.Task;

import java.util.List;

@Repository
public interface ITaskRepository extends IRepository<Task> {

    @NotNull
    List<Task> findAllByUserId(@NotNull String userId);

    void deleteAllByUserId(final @NotNull String userId);

    void deleteOneByUserIdAndId(@NotNull String userId, @NotNull String id);

}