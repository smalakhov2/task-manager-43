package ru.malakhov.tm.enumeration;

import org.jetbrains.annotations.Nullable;

public enum Role {

    USER("Пользователь"),
    ADMIN("Администратор");

    private final String displayName;

    Role(final @Nullable String displayName) {
        this.displayName = displayName;
    }

    public @Nullable String getDisplayName() {
        return displayName;
    }

}